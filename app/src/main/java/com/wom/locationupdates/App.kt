package com.wom.locationupdates

import android.app.Application


val prefs: Prefs by lazy {
    App.prefs
}

class App : Application() {
    companion object {
        lateinit var prefs: Prefs
    }

    override fun onCreate() {
        super.onCreate()
        prefs =
            Prefs(
                applicationContext
            )
    }
}
