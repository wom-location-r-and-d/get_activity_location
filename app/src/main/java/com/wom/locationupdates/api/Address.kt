package com.wom.locationupdates.api

/*
   This is the Address Model for Google Location details.
   These fields of data will be fetched from Google when user has a movement
 */

data class Address(
    val country: String,
    val country_code: String,
    val postcode: String,
    val road: String,
    val state: String,
    val state_district: String,
    val village: String
)