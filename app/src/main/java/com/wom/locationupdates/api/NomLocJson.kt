package com.wom.locationupdates.api
/*
   This is the Address Model for OSM address details.
   These fields of data will be fetched from OSM when user has a movement
 */

data class NomLocJson(
    val address: Address,
    val addresstype: String,
    val boundingbox: List<String>,
    val category: String,
    val display_name: String,
    val importance: Double,
    val lat: String,
    val licence: String,
    val lon: String,
    val name: String,
    val osm_id: Int,
    val osm_type: String,
    val place_id: Int,
    val place_rank: Int,
    val type: String
)