/*
 * Copyright (C) 2021 WOM Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wom.locationupdates.data.db

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.text.DateFormat
import java.util.Date
import java.util.UUID

/**
 * Data class for Location related data (only takes what's needed from
 * {@link android.location.Location} class).
 */

//TODO: This piece of code will be updated at the of source code updates as we need to this for debugging now
@Entity(tableName = "my_location_table")
data class MyLocationEntity(
    @PrimaryKey val id: UUID = UUID.randomUUID(),
    val latitude: Double = 0.0,
    val longitude: Double = 0.0,
    val foreground: Boolean = true,
    val date: Date = Date(),
    val address_google: String = "No Location",
    val address_nominatim: String = "No Location",
    val user_activity: String = "Unknown"
) {

    override fun toString(): String {
        val appState = if (foreground) {
            " While app opened"
        } else {
            "In Background track"
        }

        return "******* $appState:: *******\n Lat and Long: $latitude, $longitude "+
                ", \n\nAddress from google: $address_google"+
                ", \n\nAddress from Nominatim: $address_nominatim"+
                ", \n\nUser Activity: $user_activity"+
                "\n\n on date: " + "${DateFormat.getDateTimeInstance().format(date)}.\n\n\n"
    }
}
