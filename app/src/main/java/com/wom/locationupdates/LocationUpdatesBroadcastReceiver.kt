/*
 *  Copyright 2021 The Android Open Source Project [WOM].
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.wom.locationupdates

import android.app.ActivityManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.google.android.gms.location.LocationResult
import com.wom.locationupdates.data.LocationRepository
import com.wom.locationupdates.data.db.MyLocationEntity
import java.util.Date
import java.util.concurrent.Executors
import java.util.Locale
import android.location.Geocoder
import android.location.Address
import kotlinx.coroutines.*
import retrofit2.Retrofit
import retrofit2.awaitResponse
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception



private const val TAG = "LUBroadcastReceiver"

/**
 * Receiver for handling location updates.
 *
 * For apps targeting API level O and above
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)} should be used when
 * requesting location updates in the background. Due to limits on background services,
 * {@link android.app.PendingIntent#getService(Context, int, Intent, int)} should NOT be used.
 *
 *  Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 *  less frequently than the interval specified in the
 *  {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 *  foreground.
 */
class LocationUpdatesBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Log.d(TAG, "onReceive() context:$context, intent:$intent")
        GlobalScope.launch(Dispatchers.IO) {

            if (intent.action == ACTION_PROCESS_UPDATES) {
                if (prefs.token.toString().contains("Transition")) {
                    LocationResult.extractResult(intent)?.let { locationResult ->
                        val locations = locationResult.locations.map { location ->
                            MyLocationEntity(
                                latitude = location.latitude,
                                longitude = location.longitude,
                                foreground = isAppInForeground(context),
                                date = Date(location.time),
                                address_google = getAddress_google(
                                    context,
                                    location.latitude,
                                    location.longitude
                                ),
                                address_nominatim = getAddress_nominatim(
                                    context,
                                    location.latitude,
                                    location.longitude
                                ),

                                user_activity = prefs.token.toString()
                            )
                        }
                        if (locations.isNotEmpty()) {
                            LocationRepository.getInstance(context, Executors.newSingleThreadExecutor())
                                    .addLocations(locations)
                            prefs.token = "UNKNOWN_STATE"
                        }
                    }
                }
            }
        }
    }

    private fun getAddress_google(context: Context, lat:Double, long:Double):String{

        var add = "NO_ADDRESS"
        val geocoder = Geocoder(context, Locale.getDefault())

        // Address found using the Geocoder.
        var addresses: List<Address> = emptyList()
        addresses = geocoder.getFromLocation(lat, long, 1)

        // Handle case where no address was found.
        if (!addresses.isEmpty()) {
            val address = addresses[0]

             val addressFragments = with(address) {
                (0..maxAddressLineIndex).map { getAddressLine(it) }
            }

            add = addressFragments.joinToString(separator = "\n")
        }

        return add
    }

    private suspend fun getAddress_nominatim(context: Context, lat:Double, long:Double):String{

        var add = "NO_ADDRESS"
        var url = "https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat="+lat+"&lon="+long
        var base_url = "https://nominatim.openstreetmap.org/"
        val api = Retrofit.Builder()
            .baseUrl(base_url)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(ApiRequest::class.java)

       GlobalScope.launch(Dispatchers.IO) {
           try {
               val response = api.getLocation(url).awaitResponse()

               if (response.isSuccessful) {
                   val data = response.body()!!
                   add =  "(" +  data.type + ")" + data.display_name
               }
           }catch(e: Exception){
               add = "NETWORK_NOT_AVAILABLE"
           }
        }
        delay(2000)
        return add
    }

    // NOTE: TODO: THIS FUNCTION WILL BE DELETED ONCE COMPLETE DEVELOPMENT ACTIVITIES
    // Note: This function's implementation is only for debugging purposes. If you are going to do
    // this in a production app, you should instead track the state of all your activities in a
    // process via android.app.Application.ActivityLifecycleCallbacks's
    // unregisterActivityLifecycleCallbacks(). For more information, check out the link:
    // https://developer.android.com/reference/android/app/Application.html#unregisterActivityLifecycleCallbacks(android.app.Application.ActivityLifecycleCallbacks
    private fun isAppInForeground(context: Context): Boolean {
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val appProcesses = activityManager.runningAppProcesses ?: return false

        appProcesses.forEach { appProcess ->
            if (appProcess.importance ==
                ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND &&
                appProcess.processName == context.packageName) {
                return true
            }
        }
        return false
    }

    companion object {
        const val ACTION_PROCESS_UPDATES =
            "com.wom.locationupdates.action." +
                    "PROCESS_UPDATES"
    }
}
