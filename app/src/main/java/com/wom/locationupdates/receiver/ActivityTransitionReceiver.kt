package com.wom.locationupdates.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.media.session.PlaybackStateCompat
import android.telephony.ServiceState
import androidx.lifecycle.ViewModelProviders
import com.google.android.gms.location.ActivityTransitionResult
import com.wom.locationupdates.App.Companion.prefs
import com.wom.locationupdates.R
import com.wom.locationupdates.databinding.FragmentLocationUpdateBinding
import com.wom.locationupdates.ui.LocationUpdateFragment
import com.wom.locationupdates.ui.MainActivity
import com.wom.locationupdates.ui.PermissionRequestFragment
import com.wom.locationupdates.ui.PermissionRequestType
import com.wom.locationupdates.util.ActivityTransitionsUtil
import com.wom.locationupdates.viewmodels.LocationUpdateViewModel
import kotlinx.android.synthetic.main.fragment_location_update.*
import java.text.SimpleDateFormat
import java.util.*

class ActivityTransitionReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {

        if (ActivityTransitionResult.hasResult(intent)) {
            val result = ActivityTransitionResult.extractResult(intent)
            result?.let {
                result.transitionEvents.forEach { event ->
                    //TODO: This wil be updated again at the end of source code updates as we may need to for debugging now
                    val info =
                        "Transition: " + ActivityTransitionsUtil.toActivityString(event.activityType) +
                                " (" + ActivityTransitionsUtil.toTransitionType(event.transitionType) + ")" + "   " +
                                SimpleDateFormat("HH:mm:ss", Locale.US).format(Date())
                    prefs.token = info

                }
            }
        }

        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            
            locationUpdateViewModel.startLocationUpdates()
            (activity as MainActivity).requestForUpdates()
        }
    }
}