package com.wom.locationupdates

import com.wom.locationupdates.api.NomLocJson
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url




interface ApiRequest {
    @GET
    fun getLocation(@Url url: String?): Call<NomLocJson>
}