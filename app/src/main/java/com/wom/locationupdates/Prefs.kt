package com.wom.locationupdates


import android.content.Context
import android.content.SharedPreferences

class Prefs(context: Context) {
    private val PREFS_FILENAME = "WOMSharedPref"
    private val PREFS_TOKEN = "token"
    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    var token: String?
        get() = prefs.getString(PREFS_TOKEN, "UNKNOWN_STATE")
        set(value) = prefs.edit().putString(PREFS_TOKEN, value).apply()
}